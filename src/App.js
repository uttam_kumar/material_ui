import React, { Component } from "react";
import "./App.css";
import "./Styler.css";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';

import ContainedButtons from "./ContainedButtons"

export default class App extends Component {
  render() {
    const basicGridJsx = (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className="paper">xs=12</Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className="paper">xs=6</Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className="paper">xs=6</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className="paper">xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className="paper">xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className="paper">xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className="paper">xs=3</Paper>
        </Grid>
      </Grid>);

    const gridWithBreakpointsJsx = (
    <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className="paper">xs=12</Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className="paper">xs=12 sm=6</Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className="paper">xs=12 sm=6</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className="paper">xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className="paper">xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className="paper">xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className="paper">xs=6 sm=3</Paper>
        </Grid>
      </Grid>);

    const gridAutoLayoutJsx = (<div>
        <Grid container spacing={3}>
          <Grid item xs>
            <Paper className="paper">xs</Paper>
          </Grid>
          <Grid item xs>
            <Paper className="paper">xs</Paper>
          </Grid>
          <Grid item xs>
            <Paper className="paper">xs</Paper>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs>
            <Paper className="paper">xs</Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className="paper">xs=6</Paper>
          </Grid>
          <Grid item xs>
            <Paper className="paper">xs</Paper>
          </Grid>
        </Grid>
      </div>);

    const nestedGridJsx = (<div className="root">
      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={3}>
          <FormRow />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <FormRow />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <FormRow />
        </Grid>
      </Grid>
    </div>)

    const complexGridJsx = (<div className="root">
      <Paper className="paper-complex-grid">
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className="image">
              <img className="img" alt="complex" src="https://tinyjpg.com/images/social/website.jpg" />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1">
                  Standard license
                </Typography>
                <Typography variant="body2" gutterBottom>
                  Full resolution 1920x1080 • JPEG
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  ID: 1030114
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                  Remove
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant="subtitle1">$19.00</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>)


function FormRow() {
  return (
    <React.Fragment>
      <Grid item xs={4}>
        <Paper className="paper">item</Paper>
      </Grid>
      <Grid item xs={4}>
        <Paper className="paper">item</Paper>
      </Grid>
      <Grid item xs={4}>
        <Paper className="paper">item</Paper>
      </Grid>
    </React.Fragment>
  );
}

    return (
      <div className="App">
        <br />
        <span className="content-heading">Basic Grid</span>
        <br />
        <br />
        {basicGridJsx}
        <br />
        <br />
        <span className="content-heading">Grid with BreakPoints</span>
        <br />
        <br />
        {gridWithBreakpointsJsx}
        <br />
        <br />
       <span className="content-heading">Grid with Auto layout</span>
        <br />
        <br />
        {gridAutoLayoutJsx}
        <br />
        <br />
      <span className="content-heading">Complex Grid</span>
        <br />
        <br />
        {complexGridJsx}
        <br />
        <br />
      <span className="content-heading">Nested Grid</span>
        <br />
        <br />
        {nestedGridJsx}
        <br />
        <br />
        <ContainedButtons/>
        <br />
        <br />
      </div>
    );
  }
}
