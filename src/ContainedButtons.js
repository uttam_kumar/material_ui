import React, { Component } from "react";
import Button from "@material-ui/core/Button";
// import Fab from '@material-ui/core/Fab';
// import AddIcon from '@material-ui/icons/Add';
// import EditIcon from '@material-ui/icons/Edit';
// import DeleteIcon from '@material-ui/icons/Delete';
// import NavigationIcon from '@material-ui/icons/Navigation';


export default class ContainedButtons extends Component {
  render() {
    const containedButtonJsx = (
      <div>
        <Button variant="contained" className="button-style">
          Default
        </Button>
        <Button variant="contained" color="primary" className="button-style">
          Primary
        </Button>
        <Button variant="contained" color="secondary" className="button-style">
          Secondary
        </Button>
        <Button
          variant="contained"
          color="secondary"
          disabled
          className="button-style"
        >
          Disabled
        </Button>
        <Button
          variant="contained"
          href="#contained-buttons"
          className="button-style"
        >
          Link
        </Button>
        <input
          accept="image/*"
          className="input"
          id="contained-button-file"
          multiple
          type="file"
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" component="span" className="button-style">
            Upload
          </Button>
        </label>
      </div>
    );
    const textButtonJsx = (<div>
        <Button className="button-style">Default</Button>
        <Button color="primary" className="button-style">
          Primary
        </Button>
        <Button color="secondary" className="button-style">
          Secondary
        </Button>
        <Button disabled className="button-style">
          Disabled
        </Button>
        <Button href="#text-buttons" className="button-style">
          Link
        </Button>
        <input
          accept="image/*"
          className="input"
          id="text-button-file"
          multiple
          type="file"
        />
        <label htmlFor="text-button-file">
          <Button component="span" className="button-style">
            Upload
          </Button>
        </label>
      </div>)


const outlineButtonJsx = (<div>
    <Button variant="outlined" className="button-style">
      Default
    </Button>
    <Button variant="outlined" color="primary" className="button-style">
      Primary
    </Button>
    <Button variant="outlined" color="secondary" className="button-style">
      Secondary
    </Button>
    <Button variant="outlined" disabled className="button-style">
      Disabled
    </Button>
    <Button variant="outlined" href="#outlined-buttons" className="button-style">
      Link
    </Button>
    <input
      accept="image/*"
      className="input"
      id="outlined-button-file"
      multiple
      type="file"
    />
    <label htmlFor="outlined-button-file">
      <Button variant="outlined" component="span" className="button-style">
        Upload
      </Button>
    </label>
    <Button variant="outlined" color="inherit" className="button-style">
      Inherit
    </Button>
  </div>)
//needed icon
//   const fabButtonJsx = (<div>
//     <Fab color="primary" aria-label="add" className="fab">
//       <AddIcon />
//     </Fab>
//     <Fab color="secondary" aria-label="edit" className="fab">
//       <EditIcon />
//     </Fab>
//     <Fab variant="extended" aria-label="delete" className="fab">
//       <NavigationIcon className="extendedIcon" />
//       Extended
//     </Fab>
//     <Fab disabled aria-label="delete" className="fab">
//       <DeleteIcon />
//     </Fab>
//   </div>)

    return <div>
      <br />
        <br />
      <span className="content-heading">Contained Button</span>
        <br />
        <br />
        {containedButtonJsx}
        <br />
        <br />
        <span className="content-heading">Text Buttons</span>
        <br />
        <br />
        {textButtonJsx}
        <span className="content-heading">Outline Text Buttons</span>
        <br />
        <br />
        {outlineButtonJsx}
        
    </div>;
  }
}
